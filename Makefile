CC=gcc -c -Wall
bin/programaCola: obj/main.o obj/funciones.o 
	gcc obj/funciones.o obj/main.o -o $@

obj/funciones.o: src/funciones.c
	$(CC) src/funciones.c -o $@

obj/main.o: src/main.c
	$(CC) -fsanitize=address,undefined -I include/ src/main.c -o $@
