#include <stdlib.h>
#include <stdio.h>
typedef struct colaTDA{
        unsigned long tamano;
        struct nodo_colaTDA *inicio;
        struct nodo_colaTDA *fin;
}cola;

typedef struct nodo_colaTDA{
        void *elemento;
        struct nodo_colaTDA *siguiente;
        struct nodo_colaTDA *anterior;
}nodo_cola;

cola *crear_cola(){
cola colaNueva;
colaNueva.tamano=0;
cola *colaNuevaptr= &colaNueva;
return colaNuevaptr;
}

int encolar(cola *mi_cola, void *elemento){
	printf("entrada a funcion\n");
	struct nodo_colaTDA *nodoNuevo=calloc(1,sizeof(nodo_cola));
	if(nodoNuevo == NULL){
 		return -1;
	}else{
 		nodoNuevo->elemento= elemento;
  		if(mi_cola->tamano==0){
   			(mi_cola->tamano)++;
			nodoNuevo->siguiente= NULL;
			nodoNuevo->siguiente=NULL;	
   			mi_cola->inicio= nodoNuevo;
   			mi_cola->fin= nodoNuevo;
 			return 0;
  		}else{
		printf("Arreglo con al menos 1 elemento...... intentando encolar \n");
		(*(mi_cola->fin)).siguiente=nodoNuevo;
   		nodoNuevo->anterior=mi_cola->fin;
		mi_cola->fin=nodoNuevo;
		(mi_cola->tamano)++;
 		return 0;
   		}	
 	}
}

void *decolar(cola *mi_cola){
	if(mi_cola->tamano != 0){
	nodo_cola *siguienteNodo= (mi_cola->inicio);	
	void *elemento= siguienteNodo->elemento;
	siguienteNodo->anterior=NULL;
	mi_cola->inicio=siguienteNodo;
	free(mi_cola->inicio);
	(mi_cola->tamano)--;
	return elemento;	
	}else{
	return NULL;
	}
}

unsigned long tamano_cola(cola *mi_cola){
		return mi_cola->tamano;
}

unsigned long posicion_cola(cola *mi_cola, void *elemento){
	nodo_cola *direccionNodo=mi_cola->inicio;
	for(int i=0;i<mi_cola->tamano;i++){
		if(elemento== direccionNodo->elemento){
		return i;
		}	
	}
	return -1;
}

int destruir_cola(cola *mi_cola){
	long iteracion= mi_cola->tamano;  
	for(int i=0;i<iteracion;i++){
		void *elemento=decolar(mi_cola);
		if(elemento== NULL){
		return 0;
		}
	}
	return -1;
}
